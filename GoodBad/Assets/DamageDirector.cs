using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DamageDirector : MonoBehaviour
{
    Image hpGauge;

    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge").GetComponent<Image>();
    }

    public void DecreaseHp()
    {
        this.hpGauge.fillAmount -= 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        if(hpGauge.fillAmount <= 0)
        {
            SceneManager.LoadScene("GameoverScene");
        }
    }

    
}
