using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageGenerator : MonoBehaviour
{
    public GameObject DamagePrefab;
    float span = 0.3f;
    float delta = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(DamagePrefab) as GameObject;
            float px = Random.Range(-7.5f, 15.5f);
            go.transform.position = new Vector3(45, px, 0);
        }
    }
}
