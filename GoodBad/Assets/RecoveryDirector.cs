using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecoveryDirector : MonoBehaviour
{
    Image hpGauge;

    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RecoveryHp()
    {
        this.hpGauge.fillAmount += 0.1f;
    }
}
